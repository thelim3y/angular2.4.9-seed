import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StyleGuidePageComponent } from './pages/style-guide-page/style-guide-page.component';

const routes: Routes = [
  {
    path: '',
    component: StyleGuidePageComponent,
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
